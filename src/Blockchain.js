let Block = require("./Block.js");
let Transaction = require("./Transaction.js");
const _ = require("lodash");
const QuickEncrypt = require('quick-encrypt')

// Put this shit in config
//const GENESIS_ADDRESS = '-----BEGIN RSA PUBLIC KEY-----\nMIGJAoGBAMoYYO5eRZdGBw9ngJt+hCcdXE7e8bqim1m1ANQ2EVg+OZM0WBJPrgnbo4VmBODx\n4OWofBRr0e823GcZo14Wx+/tcu9095BUxefr5d+krrl4wjeYGshjbRpgHYfHYijKWztOfUFt\nmu4K0VwV9gFnVlzv8/A411MVSPpwOBxyDpivAgMBAAE=\n-----END RSA PUBLIC KEY-----\n';
// Private key of this is
const GENESIS_ADDRESS = '-----BEGIN RSA PRIVATE KEY-----\nMIICXgIBAAKBgQDKGGDuXkWXRgcPZ4CbfoQnHVxO3vG6optZtQDUNhFYPjmTNFgST64J26OF\nZgTg8eDlqHwUa9HvNtxnGaNeFsfv7XLvdPeQVMXn6+XfpK65eMI3mBrIY20aYB2Hx2Ioyls7\nTn1BbZruCtFcFfYBZ1Zc7/PwONdTFUj6cDgccg6YrwIDAQABAoGBAMKCZkDmYSpwzvjHkKOY\nSE9xVdffgl80hDr6GDsnctgww69nsOCm6gY47JDHGSj8BI5l9E6fW6ZWchmVNoSnfnQ7P85g\ndpZZJ0AGCP8NbsBM6trMhqx6PhDrbmzQjDyIWjkKRcO3GD1fVWx1Dv88gNT9YjMJ0Ji+BBWi\nOpByxkShAkEA69Ievi9nridScw3ZwITgDnEPYVYjq4ql0oOGbffu/IGAwM+KHQFdDYEwuVrn\nFXd9kgsBQjtDIje/ZqgYKd+pnwJBANtjd9GiHWjnK4TUH1Yl6/S9oGXPBFuhyCxQKMcsdWvq\nQe2EfVO9yW/yfLWAidzKMDEN4nmkCw1qftFPfc8r1vECQBd7fvA1TD+9TqqCUJKpKJ6Hh56L\ns1WbotOJoVNGAxhF1QpeIESvJBL6MF3kvWoX0fs++UsdF5xKrNaOGong6cUCQQDZgdlWNmLw\nEVjdHS1gTMKN577xkPgiHNiHzoVPSq2z6xGn+VYDw0LGQWQQ0rpb8v0qdTiXHuNyTqYiwKvb\nzPwRAkEAvg8eIzT4mTkOIAxIztuE4Mmsv1pySxk/HNLUcHiAsCRUwVWmo2ReVqTCH6xyueQ5\nhiCQWuMAQ7PgFY90NEAaAA==\n-----END RSA PRIVATE KEY-----\n'
const GENESIS_AMOUNT = 100;


class Blockchain{
    constructor(chain){
		//difficulty calculated from time taken to mine prev 2016 blocks
		//if manually forging a lower difficulty, time taken to mine prev 2016 blocks
		//now becomes shorter and leads to a higher difficulty in the next 2016 blocks
        this.difficulty = 4;
        this.pendingTransactions = [];
        this.miningReward = 100;
        this.chain = chain || [this.createGenesisBlock()];
    }

    createGenesisBlock(){
        let genesisBlock = new Block("01/01/2018", "The Beginning of All", "0");
        genesisBlock.transactions = [new Transaction(null, GENESIS_ADDRESS, GENESIS_AMOUNT)]
        genesisBlock.mineBlock(this.difficulty);
        return genesisBlock;
    }

    getLatestBlock(){
        return this.chain[this.chain.length - 1];
    }


    minePendingTransactions(miningRewardAddress){
        const rewardTransaction = new Transaction(null, miningRewardAddress, this.miningReward);
        this.pendingTransactions.push(rewardTransaction);

        //add all pending transactions into the new block
        let block = new Block(Date.now(), this.pendingTransactions, this.getLatestBlock().hash);
        block.mineBlock(this.difficulty);

        console.log("Block successfully mined!");
        this.chain.push(block);

        //pay miner for mining the new block
        this.pendingTransactions = [];
    }

    createTransaction(transaction){
        if(!this.checkTransactionValidity(transaction)) {
          console.log("Error! not enough funds in address: " + transaction.fromAddress);
          return;
        }
        this.pendingTransactions.push(transaction);
    }

    checkTransactionValidity(transaction, previousBlockHash){
        let senderBalance = this.getBalanceOfAddress(transaction.fromAddress, previousBlockHash);
        if(senderBalance < transaction.amount) return false;
        if(!transaction.verifySignature()) return false;
        return true;
    }

    getBalanceOfAddress(address, headBlockHash){
        let balance = 0;

        //go through all blocks in the chain and find blocks with transactions involving
        //this address
        if(headBlockHash) {
          let indexOfheadHash = _.findIndex(this.chain, function(block){ return block.hash === headBlockHash});
        }
        let currentChain = headBlockHash ? this.chain.slice(0, indexOfheadHash ) : this.chain;
        for(const block of currentChain){
            for(const trans of block.transactions){
                if(trans.fromAddress === address){
                    balance -= trans.amount;
                }

                if(trans.toAddress === address){
                    balance += trans.amount;
                }
            }
        }

        return balance;
    }

    isChainValid(){
        for(let i = 1; i < this.chain.length; i++){
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i - 1];

            if(currentBlock.hash !== currentBlock.calculateHash()){
                return false;
            }

            if(currentBlock.previousHash !== previousBlock.hash){
                return false;
            }

            for(const transaction of currentBlock.transactions) {
              let validity = checkTransactionValidity(transaction, previousBlock.hash);
              if(!validity){
                return false;
              }
            }
        }

        return true;
    }
	
	replaceChain(newChain){
		this.chain = newChain;
	}

    static generateKeyPair() {
      // Generate RSA private key (public key included)
      var keyPair = QuickEncrypt.generate(1024);

      return { // This is inverted we encrypt with the private key and decrypt with the public key
        private: keyPair.public,
        public: keyPair.private
      }
    }

}

module.exports = Blockchain;