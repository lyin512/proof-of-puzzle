const SHA256 = require('crypto-js/sha256');
const QuickEncrypt = require('quick-encrypt')

class Transaction{
    constructor(fromAddress, toAddress, amount, privateKey){
        this.fromAddress = fromAddress;
        this.toAddress = toAddress;
        this.amount = amount;
        if(fromAddress === null) {
          this.signature = null;
        } else {
          this.signature = this.signTransaction(privateKey);
        }
        console.log(this.signature);
    }

    verifySignature() {
      if(this.fromAddress === null) return;
      let result = QuickEncrypt.decrypt(this.signature, this.fromAddress);
      return result === this.getSha();
    }

    // NOTE: This is a client side process
    signTransaction(privateKey) {
      return QuickEncrypt.encrypt(this.getSha(), privateKey);
    }

    getSha() {
      return SHA256(this.fromAddress + this.toAddress + this.amount).toString();
    }
}

module.exports = Transaction;