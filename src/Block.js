const SHA256 = require('crypto-js/sha256');

class Block{
    //index is optional, tells where the block sits on the chain
    //timestamp is when the block was created
    //previousHash contains the previous hash of the block before this
    constructor(timestamp, transactions, previousHash = ''){
        this.timestamp = timestamp;
        this.transactions = transactions;
        this.previousHash = previousHash;
        //hash of current block is calculated when it is added to the chain
        //taking into account the previous block's hash and this block's properties
        this.hash = '';
        this.nonce = 0;
    }

    //calculate hash of this block by taking the properties of this block
    calculateHash(){
        return SHA256(this.previousHash + this.timestamp + JSON.stringify(this.transactions) + this.nonce).toString();
    }

    //proof of work, without this, a block can be tampered with and by recalculating hashes of all subsequent blocks can still lead to a valid chain
    mineBlock(difficulty){
        //by repeatedly generating new hashes by changing the nonce value such that
        //the newly generated hash's first (difficulty) number of characters are all 0's
        while(this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")){
            this.nonce++;
            this.hash = this.calculateHash();
        }

        console.log("Block mined: " + this.hash);
    }
}

module.exports = Block;