let config = global.CONFIG;
let persistenceDirPath = config.PERSISTENCE_DIRECTORY_PATH;
let fs = require('fs');

module.exports = {
	hasExistingNodesList: () => {
		if(fs.existsSync(persistenceDirPath) && fs.existsSync(persistenceDirPath + "/nodesList.txt")){
			return true;
		}else{
			return false;
		}
	},
	
	hasExistingBlockchain: () => {
		if(fs.existsSync(persistenceDirPath) && fs.existsSync(persistenceDirPath + "/blockchain.txt")){
			return true;
		}else{
			return false;
		}
	},
	
	loadNodesList: () => {
		let lineReader = require('readline').createInterface({
		  input: require('fs').createReadStream(persistenceDirPath + "/nodesList.txt")
		});

		lineReader.on('line', function (line) {
		  console.log('Line from file:', line);
		});
	},
	
	loadBlockchain: () => {
		let lineReader = require('readline').createInterface({
		  input: require('fs').createReadStream(persistenceDirPath + "/blockchain.txt")
		});

		lineReader.on('line', function (line) {
		  console.log('Line from file:', line);
		});
	}
};
