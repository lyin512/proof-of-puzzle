
let CONFIG = {}

CONFIG.PORT = 3000;
CONFIG.P2P_PORT = 9000;

CONFIG.PERSISTENCE_DIRECTORY_PATH = "../../persistence";

CONFIG.DIFFICULTY = 4;
CONFIG.HEX_BASE = 16;

// When computing the SliderPuzzle grid,
// I take a sub substing of the hex and convert to int
CONFIG.HASH_SUB_LENGTH = 10;


module.exports = CONFIG;