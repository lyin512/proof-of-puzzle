let config = global.CONFIG;
let p2p_port = config.P2P_PORT;
let WebSocket = require("ws");
let initialPeer = [];
let MessageType = {
	QUERY_CHAIN_LENGTH: 0,
    QUERY_LATEST_BLOCK: 1,
    QUERY_ENTIRE_CHAIN: 2,
	RESPONSE_CHAIN_LENGTH: 3,
	RESPONSE_LATEST_BLOCK: 4,
    RESPONSE_ENTIRE_CHAIN: 5
};

//have main create the blockchain, pass handle of blockchain into this
module.exports = function(blockchain){
	
	//starting websocket server with port specified in config
	let initP2PServer = () => {
		let server = new WebSocket.Server({port: p2p_port});
		//when a new peer tries to connect to this node, push peer into list of peers connected, init message and error handlers on the new websocket and query new peer for their current state of blockchain to see if we need to update our own
		server.on('connection', ws => initConnection(ws));
		console.log('Websocket server listening on port: ' + p2p_port);
	}

	//initialize message and error handlers for new peer connected then query new peer's chain length to determine
	//whether to update our current chain
	let initConnection = (ws) => {
		sockets.push(ws);
		initMessageHandler(ws);
		initErrorHandler(ws);
		write(ws, queryChainLengthMsg());
	}

	let initMessageHandler = (ws) => {
		ws.on('message', (data) => {
			let message = JSON.parse(data);
			console.log("Received message: " + JSON.stringify(message));
			switch(message.type){
				case MessageType.QUERY_CHAIN_LENGTH:
					write(ws, responseChainLengthMsg());
					break;
				case MessageType.QUERY_LATEST_BLOCK:
					write(ws, responseLatestBlockMsg());
					break;
				case MessageType.QUERY_ENTIRE_CHAIN:
					write(ws, responseEntireChainMsg());
					break;
				case MessageType.RESPONSE_CHAIN_LENGTH:
					handleChainLengthResponse(message);
					break;
				case MessageType.RESPONSE_LATEST_BLOCK:
					handleLatestBlockResponse(message);
					break;
				case MessageType.RESPONSE_ENTIRE_CHAIN:
					handleEntireChainResponse(message);
					break;
			}
		});
	}

	let initErrorHandler = (ws) => {
		let closeConnection = (ws) => {
			console.log('Failed connection to peer: ' + ws.url);
			sockets.splice(sockets.indexOf(ws), 1);
		};
		ws.on('close', () => closeConnection(ws));
		ws.on('error', () => closeConnection(ws));
	}

	//connect to list of peers either obtained from seed dns server or peer list obtained from previous run
	let connectToPeers = (newPeers) => {
		newPeers.forEach((peer) => {
			let ws = new WebSocket(peer);
			ws.on('open', () => {
				initConnection(ws)
			});
			ws.on('error', () => {
				console.log('Connection failed');
			});
		});
	};
	
	//query length of entire chain
	let queryChainLengthMsg = () => ({'type': MessageType.QUERY_CHAIN_LENGTH});
	
	//query peer for their latest block
	let queryLatestBlockMsg = () => ({'type': MessageType.QUERY_LATEST_BLOCK});
	
	//query peer for their entire chain
	let queryEntireChainMsg = () => ({'type': MessageType.QUERY_ENTIRE_CHAIN});
	
	//reply to a queryChainLengthMsg
	let responseChainLengthMsg = () => ({
		'type': MessageType.RESPONSE_CHAIN_LENGTH,
		'data': blockchain.chain.length
	});
	
	//reply to a queryEntireChainMsg
	let responseLatestBlockMsg = () => ({
		'type': MessageType.RESPONSE_LATEST_BLOCK,
		'data': JSON.stringify(blockchain.getLatestBlock())
	});

	//reply to a queryEntireChainMsg
	let responseEntireChainMsg = () => ({
		'type': MessageType.RESPONSE_ENTIRE_CHAIN,
		'data': JSON.stringify(blockchain.chain)
	});

	//handle reponse to chain length query
	let handleChainLengthResponse = (message) => {
		let receivedChainLength = message.data;
		if(receivedChainLength > blockchain.chain.length){
			if(receivedChainLength - blockchain.chain.length === 1){
				write(ws, queryLatestBlockMsg());
			}else{
				write(ws, queryEntireChainMsg());
			}
		}
	};

	//handle response to latest block query
	let handleLatestBlockResponse = (message) => {
		let receivedBlock = message.data;
		if(blockchain.getLatestBlock().hash === newBlockchain.getLatestBlock().previousHash){
			blockchain.addBlock(newBlockchain.getLatestBlock());
		}
	};
	
	//handle response to entire chain query
	let handleEntireChainResponse = (message) => {
		//receivedChain being an array of Blocks
		var receivedChain = message.data;
		
		//if the blockchain received is longer than the blockchain held
		if(receivedChain.length > blockchain.chain.length){
			//verify this longer chain is indeed valid
			let newBlockchain = new Blockchain(receivedChain);

			if(newBlockchain.isChainValid()){
				if(blockchain.getLatestBlock().hash === newBlockchain.getLatestBlock().previousHash){
					blockchain.addBlock(newBlockchain.getLatestBlock());
				}else{
					blockchain.replaceChain(receivedChain);
				}		
			}
		}//else ignore coz receivedChain is shorter than chain held
	};
	
	let broadcastNewBlockMsg = () => {
		broadcast(responseLatestBlockMsg());
	};
	
	let write = (ws, message) => ws.send(JSON.stringify(message));
	let broadcast = (message) => sockets.forEach(socket => write(socket, message));
	
	let querySeedDNSServerForNodes = () => {
		return [];
	};
	
	return {
		initP2PServer: initP2PServer,
		connectToPeers: connectToPeers,
		broadcastNewBlockMsg: broadcastNewBlockMsg,
		querySeedDNSServerForNodes: querySeedDNSServerForNodes
	};
};








