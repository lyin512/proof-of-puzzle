'use strict'

const crypto = require('crypto');


const CONSTANTS = {
    DIFFICULTY: 4,
    HEX_BASE: 16,

    // When computing the SliderPuzzle grid,
    // I take a sub substing of the hex and convert to int
    HASH_SUB_LENGTH: 10
}

const CONFIG = global.CONFIG || CONSTANTS;

class SliderPuzzle{

    // dimenmrtions are the length of x and y of the slider puzzle.
    // slider puzzle with allways be a square

    constructor(hash, dufficulty){

        this.hash = hash;
        this.dufficulty = dufficulty;
        this.grid = this.createGrid(hash, dufficulty)
    }

    createGrid(hash, dufficulty) {

        let grid = []; // the slider puzzle's layout is a grid of numbers.

        // length is the area of the grid (x * y).. the grid is always a square. so can do x^2 for short hand.
        // the number of tiles is 1 less than the grid size hence (Math.pow(dufficulty, 2) - 1)
        // the lambda function populates the array with ints ranging from 1 to (grid area - 1)
        let possibleNumbers = Array.from({length: (Math.pow(dufficulty, 2) - 1)}, (v, i) => (i + 1) );

        while(possibleNumbers.length > 0){

            console.log("possibleNumbers: " + possibleNumbers);
            
            const positionDeterminedByHash  = parseInt(hash.substring(0, CONFIG.HASH_SUB_LENGTH), CONFIG.HEX_BASE) % (possibleNumbers.length)
            console.log("positionDeterminedByHash: " + positionDeterminedByHash);

            // selectedNumber is the value that is pulled from the possibleNumbers.
            // analogy: spin the wheel, when the wheel stops, that is the selectedNumber.
            const selectedNumber = possibleNumbers[positionDeterminedByHash];

            grid.push(selectedNumber)
            possibleNumbers.splice(positionDeterminedByHash, 1);
            console.log("selectedNumber: " + selectedNumber);

            hash = crypto.createHmac('sha256', hash + selectedNumber).digest('hex');
            console.log("hash: " + hash);

            console.log("grid: " + grid);
            console.log("\n");

        }

        // 0 will represent the missing tile,
        // the puzzle will always start with the missing tile in the bottom right corner.
        grid.push(0);

    }

    convert1Dto2D(_1Darr, dimention){
        // dimention of the square 2D array is the same as difficulty
        var _2Darr = [];
        while(_1Darr.length)
            _2Darr.push(_1Darr.splice(0, dimention));

        return _2Darr;
    }


    moveTile(){

    }


    posilbeMovesForTiles(){
        const emptyTile = findEmptyTileLocation();
        const min = 0;
        const max = this.dufficulty;




    }

    findEmptyTileLocation(){
        const grid2D = convert1Dto2D(this.grid, this.dufficulty);
        
        for(var y = 0; y < grid2D.length; y++){

            for(var x = 0; x < grid2D[y].length; x++){

                if(this.grid[y][x] == 0)
                    return {'x': x, 'y': y};
            }

        }
    }

    checkIfSolved(){

    }
}

module.exports = SliderPuzzle;
