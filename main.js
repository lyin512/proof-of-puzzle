global.CONFIG = require('./app/config/config.js');

let Blockchain = require("./src/Blockchain.js");
let Transaction = require("./src/Transaction.js");
let express = require('./app/config/express.js');
let persistence = require("./app/persistence.js");

//check to see if a nodes(peers) address list and blockchain exist
//if a copy of blockchain and nodes list exist then connect and query them for their blockchain lengths and update accordingly
//if not, query seed dns server for addresses of nodes to connect to and obtain a copy of the chain from the nodes

let mikuCoin;
if(persistence.hasExistingBlockchain()){
	mikuCoin = persistence.loadBlockchain();
}else{
	mikuCoin = new Blockchain();
	mikuCoin.createTransaction(new Transaction("Satoshi Nakamoto", "address0002", 50));
	mikuCoin.minePendingTransactions("Satoshi Nakamoto");
	console.log(mikuCoin.chain);

	mikuCoin.createTransaction(new Transaction("address0002", "address0003", 100));
	mikuCoin.minePendingTransactions("Satoshi Nakamoto");
	console.log(mikuCoin.chain);
}

let webSocket = require('./app/config/webSocket.js')(mikuCoin);
webSocket.initP2PServer();

let nodesList;
if(persistence.hasExistingNodesList()){
	nodesList = persistence.loadNodesList();
}else{
	nodesList = webSocket.querySeedDNSServerForNodes();
}

webSocket.connectToPeers(nodesList);

const GENESIS_PRIVATE_ADDRESS = '-----BEGIN RSA PUBLIC KEY-----\nMIGJAoGBAMoYYO5eRZdGBw9ngJt+hCcdXE7e8bqim1m1ANQ2EVg+OZM0WBJPrgnbo4VmBODx\n4OWofBRr0e823GcZo14Wx+/tcu9095BUxefr5d+krrl4wjeYGshjbRpgHYfHYijKWztOfUFt\nmu4K0VwV9gFnVlzv8/A411MVSPpwOBxyDpivAgMBAAE=\n-----END RSA PUBLIC KEY-----\n';
let GENESIS_ADDRESS = '-----BEGIN RSA PRIVATE KEY-----\nMIICXgIBAAKBgQDKGGDuXkWXRgcPZ4CbfoQnHVxO3vG6optZtQDUNhFYPjmTNFgST64J26OF\nZgTg8eDlqHwUa9HvNtxnGaNeFsfv7XLvdPeQVMXn6+XfpK65eMI3mBrIY20aYB2Hx2Ioyls7\nTn1BbZruCtFcFfYBZ1Zc7/PwONdTFUj6cDgccg6YrwIDAQABAoGBAMKCZkDmYSpwzvjHkKOY\nSE9xVdffgl80hDr6GDsnctgww69nsOCm6gY47JDHGSj8BI5l9E6fW6ZWchmVNoSnfnQ7P85g\ndpZZJ0AGCP8NbsBM6trMhqx6PhDrbmzQjDyIWjkKRcO3GD1fVWx1Dv88gNT9YjMJ0Ji+BBWi\nOpByxkShAkEA69Ievi9nridScw3ZwITgDnEPYVYjq4ql0oOGbffu/IGAwM+KHQFdDYEwuVrn\nFXd9kgsBQjtDIje/ZqgYKd+pnwJBANtjd9GiHWjnK4TUH1Yl6/S9oGXPBFuhyCxQKMcsdWvq\nQe2EfVO9yW/yfLWAidzKMDEN4nmkCw1qftFPfc8r1vECQBd7fvA1TD+9TqqCUJKpKJ6Hh56L\ns1WbotOJoVNGAxhF1QpeIESvJBL6MF3kvWoX0fs++UsdF5xKrNaOGong6cUCQQDZgdlWNmLw\nEVjdHS1gTMKN577xkPgiHNiHzoVPSq2z6xGn+VYDw0LGQWQQ0rpb8v0qdTiXHuNyTqYiwKvb\nzPwRAkEAvg8eIzT4mTkOIAxIztuE4Mmsv1pySxk/HNLUcHiAsCRUwVWmo2ReVqTCH6xyueQ5\nhiCQWuMAQ7PgFY90NEAaAA==\n-----END RSA PRIVATE KEY-----\n';

var newKeyPair = Blockchain.generateKeyPair();

mikuCoin.createTransaction(new Transaction(GENESIS_ADDRESS, newKeyPair.public, 100, GENESIS_PRIVATE_ADDRESS));

console.log(mikuCoin.isChainValid());

/*
mikuCoin.createTransaction(new Transaction("Satoshi Nakamoto", "address0002", 50));
mikuCoin.minePendingTransactions("Satoshi Nakamoto");
console.log(mikuCoin.chain);

mikuCoin.createTransaction(new Transaction("address0002", "address0003", 100));
mikuCoin.minePendingTransactions("Satoshi Nakamoto");
console.log(mikuCoin.chain);
*/

/*
console.log("\n Starting the miner...");
mikuCoin.minePendingTransactions("miku-address");

mikuCoin.minePendingTransactions("ai-chan-address");

console.log("\nBalance of Miku's wallet is: " + mikuCoin.getBalanceOfAddress("miku-address"));

mikuCoin.createTransaction(new Transaction("miku-address", "ai-chan-address", 30));

mikuCoin.createTransaction(new Transaction("miku-address", "ai-chan-address", 1000));

mikuCoin.minePendingTransactions("ai-chan-address");

console.log("\nBalance of Miku's wallet is: " + mikuCoin.getBalanceOfAddress("miku-address"));
console.log("\nBalance of Ai-chan's wallet is: " + mikuCoin.getBalanceOfAddress("ai-chan-address"));
*/


//test vishaal